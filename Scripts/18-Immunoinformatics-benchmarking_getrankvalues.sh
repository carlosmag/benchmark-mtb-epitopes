cd "$( dirname -- "$0"; )" ;
conda deactivate ; conda activate immunoinf

mkdir -p ../Immunoinformatics/iedbepitopes/benchmark ;
cd ../Immunoinformatics/iedbepitopes/benchmark ;


echo "Create launching scripts for all (n=660) DRB locus alleles" ;
#---------------------------------------------------------------------------------------  
netmhciipan -list \
| less \
| grep '^DRB' \
| less \
>hlasclass2_list_DRBlocus.txt ;
head -3 hlasclass2_list_DRBlocus.txt ;
wc -l hlasclass2_list_DRBlocus.txt ;
## 660 HLA-DRB alleles with the names reported by netMHCIIpan!
head -3 hlasclass2_list_DRBlocus.txt ;
wc -l hlasclass2_list_DRBlocus.txt ;

DRBhlas=$(less hlasclass2_list_DRBlocus.txt) ;
## 10threads: 660/10=66 -> so we can put 66 HLAs into each script

j=0 ;
for i in $(seq 1 66 660) ;
  do echo i="$i" ;
    ((j++)) ;
    
    hlas_cmd=$(echo "$DRBhlas" \
    | less \
    | awk -v i="$i" 'NR>=i && NR<=i+65' ORS="," \
    | awk '{print $0}' \
    | sed -e 's/,$//g')
    echo hlas_cmd="$hlas_cmd" ;
    
    
    echo "netmhciipan -a $hlas_cmd -inptype 1 -u -f ../peps_concordantstatus_classII_tcellassays_joined.mhc2 >mhc2iedbepitopes_10cores_""$j""set.myout4" >iedbepitopes_10cores_"$j"set_mhc2.sh ;

done ;

chmod 775 *.sh ;
ls -ltrh *.sh ;
ls -ltrh *.sh | awk '{print $NF}' | xargs cat ;
## seems to have been created successfully!
unset $DRBhlas ;


echo "Run paralell netMHCIIpan instances on iedbepitopes" ;
#---------------------------------------------------------------------------------------  
time parallel -j0 bash ::: iedbepitopes_10cores_{1..10}set_mhc2.sh ;
## 17min23s for 1240 peptides x 660 HLAs!
ls -ltrh ;
cat *.myout4 | head -100 ;
cat *.myout4 | tail -20 ;
## seems to ave worked!
du -shc *.myout4 ;
## 97MB of data!


echo "Join and process immunoinformatics predictions" ;
#---------------------------------------------------------------------------------------
time find . -maxdepth 1 -name "*.myout4" \
| xargs grep -v -e '^#' -e '^Number' -e '^-' \
| less \
| sed -e '/:$/d' -e 's/[ ]\+/\t/g' -e 's/\t\t/\t/g' -e 's/^.*myout4.*DRB/mhc2iedbepitopes\tDRB/g' -e 's/.*\tPos\t/Group\t/g' \
| less \
| awk -F'\t' 'NR==1 {print $0} NR>1 && $1!="Group" {print $0}' OFS='\t' \
| less \
| awk -F'\t' '{if($11==""){$11="null"} ; print $0}' OFS='\t' \
>mhc2iedbepitopes.combined ;
head -2 mhc2iedbepitopes.combined ;
du -sh mhc2iedbepitopes.combined ; 
wc -l mhc2iedbepitopes.combined ;
## 2.5s for 75MB file with 818401 rows!!
awk -F'\t' '{print NF}' mhc2iedbepitopes.combined \
| sort -u ;
## 11 fields in all rows!!


echo "QC control immunoinformatics predictions" ;
#---------------------------------------------------------------------------------------  
time cat mhc2iedbepitopes.combined \
| cut -f2 \
| sed '1d' \
| sort -u \
| wc -l ;
## 0.3s and 660 HLAs present, confirmed!
time cat mhc2iedbepitopes.combined \
| awk -F'\t' '$2=="DRB1_0101" {print $3}' \
| sort \
| uniq -c \
| awk '$1>1' \
| wc -l ;
## no duplicated peptides, confirmed!
cat mhc2iedbepitopes.combined \
| awk -F'\t' '$2=="DRB1_0101" {print $3}' \
| sort -u \
| less \
| wc -l ;
## 1240 peptides -> same number as input!
cut -f2,3 mhc2iedbepitopes.combined \
| sed '1d' \
| datamash -s -g 1 count 2 \
| less \
| grep -v -w '1240' -c;
## 1240 peptides per HLA, confirmed!
less ../peps_concordantstatus_classII_tcellassays_joined.mhc2 \
| awk -F'\t' 'NR==FNR {peptides[$1] ; next} !($3 in peptides)' OFS='\t' /dev/stdin mhc2iedbepitopes.combined ;
## no erroneous peptides present!
cat mhc2iedbepitopes.combined \
| awk -F'\t' '$(NF-1)!="NA"' OFS='\t' ;
## no peptides with ExpBind...


echo "Evaluate status (positive and negative) for each peptide" ;
#---------------------------------------------------------------------------------------
less ../concordantstatus_classII_tcellassays_joined.tsv \
| datamash -s -g 1 unique 2,5 countunique 3,5 \
| less \
| awk -F'\t' '$NF>1 && length($1)>8' OFS='\t' \
| less \
>peptideswithdiscordantstatuswhenjoininghlas_classII_tcellassays_joined.tsv ;
cat -n peptideswithdiscordantstatuswhenjoininghlas_classII_tcellassays_joined.tsv ;
## 16 peptides are classified as both "positive" and "negative" depending on the HLA!
grep -v -e 'HLA class II' -e 'HLA-DR' peptideswithdiscordantstatuswhenjoininghlas_classII_tcellassays_joined.tsv -c ;
## all the 16 peptides have either 'HLA class II' or 'HLA-DR' as restricting HLA, so they will need to be excluded!

head -2 ../concordantstatus_classII_tcellassays_joined.tsv ;
less ../concordantstatus_classII_tcellassays_joined.tsv \
| datamash -s -g 1 unique 2,5 countunique 2,5 \
| less \
| awk -F'\t' '$NF==1 && length($1)>8' OFS='\t' \
| less \
>singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv ;
head -2 singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv ;
wc -l singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv ;
## 1224 entries, less 16 than previously...


echo "Evaluate HLA restriction data for each peptide" ;
#---------------------------------------------------------------------------------------
less singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv \
| grep '\*' \
| less \
| wc -l ;
## 162 / 1224 ≃ 13.2% of the peptides with 4-digit HLA info!

grep ',' singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv -m2 ;
grep ',' singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv -c ;
## 108 peptides with >1 restricting HLA!
head -2 hlasclass2_list_DRBlocus.txt ;
less singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv \
| grep '\*' \
| less \
| grep -v ',' \
| less \
| cut -f1-3 \
| sed -e 's/HLA-DRB/DRB/g' -e 's/\*/_/g' -e 's/://g' \
| less \
>restrictranksearchonspecificHLAs_peptides.txt ;
cat -n restrictranksearchonspecificHLAs_peptides.txt ;
## 94 / 1224 ≃ 7.7% of the peptides with only 4-digit HLA info -> better to consider the restricting HLA only!
less singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv \
| grep '\*' \
| less \
| grep ',' \
| less \
| grep -v -e 'HLA class II' -e 'HLA-DR,' \
| less \
| sed -e 's/HLA-DRB[1-9]$/HLA-DRB!/g' \
| grep -e 'DR[1-9]' -e 'DRB[1-9],' -e '!' \
| cat -n ;
## only 4 / 1224 ≃ 0.33% of the peptides with less general but non specific HLA restricting data clustered with 4-digit HLA info-> no need to bother -> consider the minimum rank value of all tested HLAs!
less singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv \
| grep '\*' \
| less \
| grep ',' \
| less \
| grep -v -e 'HLA class II' -e 'HLA-DR,' -e 'HLA-DRB1,' \
| less \
| grep -v -e 'DR[1-9]' -e 'DRB[1-9],' \
| less \
| awk -F'\t' '{split($2,hlas,",") ; status=$3 ; for(i=1;i<=length(hlas);i++){print $1,hlas[i],status}}' OFS='\t' \
| less \
| sed -e 's/HLA-DRB/DRB/g' -e 's/\*/_/g' -e 's/://g' \
| less \
>restrictranksearchonspecificandmultipleHLAs_peptides.txt ;
cat -n restrictranksearchonspecificandmultipleHLAs_peptides.txt ;
## 31 additional peptides with >1 restricting 4-digit HLA info -> better to consider the restricting HLA only!
## so, we have (94+31) / 1224 ≃ 10.2% of the peptides to consider the restricting HLA only!

less singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv \
| grep -e 'HLA-DRB[1-9]' -e 'DR[1-9]' -e 'DRB[1-9]' \
| less \
| grep -v -e 'HLA class II' -e 'HLA-DR,' \
| less \
| sed -e '/\tHLA-DRB[1-9]\*/d' \
| less \
| cut -f2 \
| sort \
| uniq -c \
| sort -nrk1,1 \
| cat -n ;
## 89 peptides with less general but non specific HLA restricting data!
### 62 / 89 ≃ 70% of these peptides are tagged with 'HLA-DRB1' as restricting data -> consider only DRB1 locus (which is the great majority anyways)
#### from the remaining 23 peptides, only 2 are clustered with other locci '(HLA-DR53' <- HLA-DRB4*01:01 and HLA-DRB4*01:03 or HLA-DRB5*01:01) -> consider the minimum rank value on all tested HLAs on these 2 peptides!
less singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv \
| grep -e 'HLA-DRB[1-9]' -e 'DR[1-9]' -e 'DRB[1-9]' \
| less \
| grep -v -e 'HLA class II' -e 'HLA-DR,' \
| less \
| sed -e '/\tHLA-DRB[1-9]\*/d' \
| less \
| grep -v -e 'HLA-DR53,' -e 'HLA-DRB5\*01:01' \
| less \
| cut -f1-3 \
| sed -e 's/HLA-DRB/DRB/g' -e 's/\*/_/g' -e 's/://g' \
| less \
>restrictranksearchonDBR1_peptides.txt ;
cat -n restrictranksearchonDBR1_peptides.txt ;
## 87 peptides to restrict search on HLA-DRB1 locus!


echo "Create table with minimum rank(%) value per peptide" ;
#---------------------------------------------------------------------------------------
## goal is to have a single value per peptide epitope...
netmhciipan -h ;
## default SBbinding threshold is 2% ; WBbinding treshold is 10%

head -2 mhc2iedbepitopes.combined ;
cat restrictranksearchonspecificHLAs_peptides.txt restrictranksearchonspecificandmultipleHLAs_peptides.txt \
| less \
| awk -F'\t' '{print $2,$1}' OFS='\t' \
| less \
| grep -w -F -f /dev/stdin mhc2iedbepitopes.combined \
| less \
>restrictranksearchonspecificHLAs_mhc2iedbepitopes_minmaxrankvalues.tsv ;
head -2 restrictranksearchonspecificHLAs_mhc2iedbepitopes_minmaxrankvalues.tsv ;
wc -l restrictranksearchonspecificHLAs_mhc2iedbepitopes_minmaxrankvalues.tsv ;
## 187 rows, coonfirmed!

head -2 mhc2iedbepitopes.combined ;
head -2 singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv ;
less mhc2iedbepitopes.combined \
| awk -F'\t' 'NR==FNR {peptide[$1] ; next} !($3 in peptide)' OFS='\t' <(cat restrictranksearchonspecificHLAs_peptides.txt restrictranksearchonspecificandmultipleHLAs_peptides.txt restrictranksearchonDBR1_peptides.txt | less) /dev/stdin \
| less \
| cat /dev/stdin restrictranksearchonspecificHLAs_mhc2iedbepitopes_minmaxrankvalues.tsv \
| less \
| cat /dev/stdin <(cut -f1 restrictranksearchonDBR1_peptides.txt | grep -F -f /dev/stdin mhc2iedbepitopes.combined | grep 'DRB1_' | less) \
| less \
| awk -F'\t' 'NR==FNR {peptide[$1] ; status[$1]=$3 ; next} ($3 in peptide){print $1,$3,status[$3],$2,$(NF-2)}' OFS='\t' singlestatusonly_concordantstatus_classII_tcellassays_joined.tsv /dev/stdin \
| less \
| datamash -s -g 1,2,3 min 5 max 5 \
| sed '1iGroup\tpepID\tStatus\tminRank\tmaxRank' \
>mhc2iedbepitopes_minmaxrankvalues.tsv ;
head mhc2iedbepitopes_minmaxrankvalues.tsv ;
wc -l mhc2iedbepitopes_minmaxrankvalues.tsv ;
ls -ltrh mhc2iedbepitopes_minmaxrankvalues.tsv ;
## 66KB file with 1224 peptides!!
awk -F'\t' '{print NF}' mhc2iedbepitopes_minmaxrankvalues.tsv \
| sort -u ;
## 5 fields in all rows!
less mhc2iedbepitopes_minmaxrankvalues.tsv \
| datamash -s -g 2 countunique 3 \
| less \
| awk -F'\t' '$NF>1' \
| wc -l ;
## all peptides with a single assigned status, confirmed!

head -2 mhc2iedbepitopes_minmaxrankvalues.tsv ;
sed '1d' mhc2iedbepitopes_minmaxrankvalues.tsv \
| datamash -s -g 1,3 median 4 mean 4 median 5 mean 5 count 2 ;
## 1021 unique positive peptides -> minRank: median=5.25 ; mean ≃ 14.82 ; maxRank: median=79.8 ; mean≃70.9
## 203 unique negative peptides -> minRank: median=9.09 ; mean ≃ 18.1 ; maxRank: median=86.08 ; mean≃78.08
### minRank median of the positive peptides is 9.09 / 5.25 ≃ 1.73 times higher than the negative peptides!!!
### so, it seems there's real a tendency separating positive and negative peptides! we just need plotting and a statistical test to prove this!


echo "Prepare dataframes for statistical tests" ;
#---------------------------------------------------------------------------------------  
head -2 mhc2iedbepitopes_minmaxrankvalues.tsv ;
less mhc2iedbepitopes_minmaxrankvalues.tsv \
| sed '1d' \
| cut -f3,4 \
| sed '1iStatus\tMinrank' \
| less \
>mhc2iedbepitopes_minrankfilter.tsv ;
head mhc2iedbepitopes_minrankfilter.tsv ;
wc -l mhc2iedbepitopes_minrankfilter.tsv ;
## 1224 peptides!


echo "Compress and remove large files" ;
#---------------------------------------------------------------------------------------
du -shc * | sort -rh ;
## 171MB total files!
ls *.myout4 *.combined \
| sed -e 's|^|./|g' \
>compressedanduploadedfiles_benchmarkmhc2iedbepitopes.txt ;
cat -n compressedanduploadedfiles_benchmarkmhc2iedbepitopes.txt ;
## 11 files to compress!
tar -czvf benchmarkmhc2iedbepitopes.tar.gz -T compressedanduploadedfiles_benchmarkmhc2iedbepitopes.txt ;
ls -ltrh benchmarkmhc2iedbepitopes.tar.gz ;
## 42MB archive uploaded to Figshare!
ls * \
| grep -v -e '.combined' -e '.myout4' -e 'tar.gz' \
| less \
| xargs du -shc ;
## 232KB without compressed files!
less compressedanduploadedfiles_benchmarkmhc2iedbepitopes.txt \
| xargs rm ;
rm benchmarkmhc2iedbepitopes.tar.gz ;

ls -ltrh ;