cd "$( dirname -- "$0"; )" ;
mkdir -p ../Immunoinformatics/iedbepitopes ;
cd ../Immunoinformatics/iedbepitopes ;


echo "Get peptide list for MHC2 predictions" ;
#--------------------------------------------------------------------------------------- 
less ../../NGS_helper_files/iedbepitopes_mhc2only_menardo2021.mhc2 \
| awk 'length($1)>8' \
| less \
>iedbepitopes_mhc2only_menardo2021_9aaormore.mhc2 ;
cat -n iedbepitopes_mhc2only_menardo2021_9aaormore.mhc2 ;
## 1143 peptide epitopes (only 1 peptide excluded)!


echo "Compare T cell epitopes from Menardo2021 to the full set in IEDB" ;
#---------------------------------------------------------------------------------------  
## epitopes extracted from iedb.org on 30/11/2021 -> Linear peptide ; Organism=Mycobacterium tuberculosis complex (same peptides as individual strains selection) ; Human host ; T cell and MHC ligand assays (positive and then negative ; MHC ligand only after exporting T cell-only results -> main epitope list is not the same when the two are combined) ; class II MHC restriction
### 6 files obtained, 1 for T cell epitope list, 1 for T cell assays and 1 for MHC ligand assays (repeat for negative) and saved as 'tcellepitopes_classII_*.csv'!
head -3 tcellepitopes_classII_positive_assays_tcell.csv | cat -n ;
head -3 tcellepitopes_classII_negative_assays_tcell.csv | cat -n ;
## 2 header rows in both files!
cat <(awk 'NR>2 {print $0,"positive"}' OFS='\t' tcellepitopes_classII_positive_assays_tcell.csv | less) <(awk 'NR>2 {print $0,"negative"}' OFS='\t' tcellepitopes_classII_negative_assays_tcell.csv | less) \
| less \
| sed -e 's/",/\t/g' -e 's/"//g' \
| less \
| awk -F'\t' '{print $12,$102,$7,$4,$NF}' OFS='\t' \
| less \
>tcellepitopes_classII_tcellassays_joined.tsv ;
head -3 tcellepitopes_classII_tcellassays_joined.tsv ;
wc -l tcellepitopes_classII_tcellassays_joined.tsv ;
## 3479 rows!
### HLA restriction data present!!
### through references tab on IEDB search, it was possible to verify that there are 4 studies (excluding PMID: 31908851) published after 03-08-2020 that were not possibly included into Menardo2021 list!
less tcellepitopes_classII_tcellassays_joined.tsv \
| grep 'positive' \
| less \
| awk -F'\t' '$3>2019' OFS='\t' \
| less \
| cut -f1 \
| sort -u \
| less \
| grep -w -F -f iedbepitopes_mhc2only_menardo2021.mhc2 /dev/stdin \
| less \
| wc -l ;
## 24 of the 43 possibly novel peptides were already considered in Menardo2021!
### 43-24=19 peptides to evaluate...
less tcellepitopes_classII_tcellassays_joined.tsv \
| cut -f1 \
| sort -u \
| less \
| grep -v -w -F -f /dev/stdin ../../NGS_helper_files/iedbepitopes_mhc2only_menardo2021.mhc2 \
| less \
| wc -l ;
## there are no novel epitopes in menardo2021 list!, but there are "negative" epitopes in menardo2021 list!


echo "Investigate whether the same epitope is simultaneously positive and negative for the same HLA" ;
#---------------------------------------------------------------------------------------  
head -3 tcellepitopes_classII_positive_assays_mhcligand.csv | cat -n ;
head -3 tcellepitopes_classII_negative_assays_mhcligand.csv | cat -n ;
## 2 header rows!
cat <(awk 'NR>2 {print $0,"mhcligand","positive"}' OFS='\t' tcellepitopes_classII_positive_assays_mhcligand.csv | less) <(awk 'NR>2 {print $0,"mhcligand","negative"}' OFS='\t' tcellepitopes_classII_negative_assays_mhcligand.csv | less) \
| less \
| sed -e 's/",/\t/g' -e 's/"//g' \
| less \
| awk -F'\t' '{print $12,$81,$(NF-1),$NF}' OFS='\t' \
| less \
| cat /dev/stdin <(awk -F'\t' '{print $1,$2,"tcell",$NF}' OFS='\t' tcellepitopes_classII_tcellassays_joined.tsv) \
| less \
| grep -v '\+' \
| less \
>tcellepitopes_classII_mhcligandandtcell_joined.tsv ;
head -3 tcellepitopes_classII_mhcligandandtcell_joined.tsv ;
wc -l tcellepitopes_classII_mhcligandandtcell_joined.tsv ;
grep -w 'mhcligand' tcellepitopes_classII_mhcligandandtcell_joined.tsv -c ;
## 7838 rows (4359 for MHC ligand assays)

less tcellepitopes_classII_mhcligandandtcell_joined.tsv \
| grep -e '-DP' -e '-DQ' -e '-DR' \
| less \
| grep -w 'positive' \
| less \
| grep '\-DR' \
| less \
| wc -l ;
### 2668 / 3644 ≃ 73.2% of the positive results with HLA restriction (tcell + mhcligand) for Mtb are from DR locus!
less tcellepitopes_classII_mhcligandandtcell_joined.tsv \
| grep -e '-DP' -e '-DQ' -e '-DR' \
| less \
| grep -w 'positive' \
| less \
| grep 'tcell' \
| less \
| grep '\-DR' \
| less \
| wc -l ;
## 874 / 1074 ≃ 81.4% of the positive results with HLA restriction in T cell assays for Mtb are from DR locus!!

less tcellepitopes_classII_mhcligandandtcell_joined.tsv \
| cut -f2 \
| sort \
| uniq -c ;
## there are many abbreviated forms that can be easily converted into 4-digit resolution HLAs!
### 'HLA class II' <- watch out for any positive+negative HLA
### 'HLA-DR' <- watch out for any positive+negative in 'HLA-DRxxxxxxxx' alleles
### 'HLA-DR1' <- HLA-DRB1*01:01 (because it is the only HLA-DRB1*01:xx present ; https://en.wikipedia.org/wiki/HLA-DR1)
### 'HLA-DR11' -> HLA-DRB1*11:01 and HLA-DRB1*11:02 (the only 2 HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR11)
### 'HLA-DR12' <- HLA-DRB1*12:01 (the only HLA present here) (https://en.wikipedia.org/wiki/HLA-DR12)
### 'HLA-DR13' <- HLA-DRB1*13:01, HLA-DRB1*13:02, HLA-DRB1*13:03 and HLA-DRB1*13:04 (the only 4 HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR13)
## 'HLA-DR14' <- HLA-DRB1*14:02 and HLA-DRB1*14:05 (the only 2 HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR14)
### 'HLA-DR15' <- HLA-DRA*01:01/DRB1*15:01, HLA-DRB1*15:01, HLA-DRB1*15:02, HLA-DRB1*15:03 and HLA-DRB1*15:04 (the only 4 HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR15)
### 'HLA-DR17' <- HLA-DRB1*03:01 (the only HLA present here) (https://en.wikipedia.org/wiki/HLA-DR17)
### 'HLA-DR2' <- HLA-DRA*01:01/DRB1*15:01, HLA-DRB1*15:01, HLA-DRB1*15:02, HLA-DRB1*15:03, HLA-DRB1*15:04 and HLA-DRB1*16:02 (the only HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR2)
### 'HLA-DR3' <- HLA-DRB1*03:01, HLA-DRB1*03:02, HLA-DRB1*03:03 and HLA-DRB1*03:05 (the only HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR3)
### 'HLA-DR4' <- HLA-DRB1*04:01, HLA-DRB1*04:04, HLA-DRB1*04:05 and HLA-DRB1*04:11 (the only HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR4)
### 'HLA-DR5' <- HLA-DRB1*11:01, HLA-DRB1*11:02 and HLA-DRB1*12:01 (the only HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR5)
### 'HLA-DR52' <- HLA-DRB3*01:01, HLA-DRB3*02:01, HLA-DRB3*02:02 and HLA-DRB3*03:01 (the only HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR52)
### 'HLA-DR53' <- HLA-DRB4*01:01 and HLA-DRB4*01:03 (the only HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR53)
### 'HLA-DR6' <- HLA-DRB1*13:01, HLA-DRB1*13:02, HLA-DRB1*13:03, HLA-DRB1*13:04, HLA-DRB1*14:02 and HLA-DRB1*14:05 (the only HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR6)
### 'HLA-DR7' <- HLA-DRB1*07:01 (the only HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR7)
### 'HLA-DR8' <- HLA-DRB1*08:01, HLA-DRB1*08:02, HLA-DRB1*08:03 and HLA-DRB1*08:04 (the only HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR8)
### 'HLA-DR9' <- HLA-DRB1*09:01 (the only HLAs present here) (https://en.wikipedia.org/wiki/HLA-DR9)
## HLA-DRB5 <- HLA-DRB5*01:01 and HLA-DRB5*01:02
## so, 'HLA-DR15' might be grouped with 'HLA-DR2' ; 'HLA-DR17' with 'HLA-DR3' ; 'HLA-DR5' with 'HLA-DR11'+'HLA-DR12' ; 'HLA-DR6' with 'HLA-DR13'+'HLA-DR14' !!

## strategy: group by peptide, filter the ones with positive+negative, watch out for the corresponding abbreviated forms grouping, including with 4-digit resolution HLAs!
head -2 tcellepitopes_classII_mhcligandandtcell_joined.tsv ;
less tcellepitopes_classII_mhcligandandtcell_joined.tsv \
| grep '\-DR' \
| less \
| datamash -s -g 1 unique 2,3,4 countunique 2,3,4 \
| less \
| grep -e '-DR[1-9]' \
| less \
| awk -F'\t' '$(NF-2)>1 && $NF>1' OFS='\t' \
| less \
| grep -e 'DR15.*DR2' -e 'DR17.*DR3' -e 'DR11.*DR5' -e 'DR12.*DR5' -e 'DR13.*DR6' -e 'DR14.*DR6' \
| cat -n ;
## only a single peptide to check!
grep -w 'KTIAYDEEARR' tcellepitopes_classII_mhcligandandtcell_joined.tsv \
| sort -k2,2 ;
## 'HLA-DRB1*03:01' is concordant with 'HLA-DR3' and 'HLA-DR17', so we will consider the individual HLA data for this peptide (although HLA-DRB1*03:02, HLA-DRB1*03:03 and HLA-DRB1*03:05 are discordant)
## conclusion: discordant status will be checked through grouping by individual HLA!


# peptides to exclude due to positive+negative status for the same HLA:
#------------------------------------------------------
head -2 tcellepitopes_classII_mhcligandandtcell_joined.tsv ;
less tcellepitopes_classII_mhcligandandtcell_joined.tsv \
| grep -e '-DR' -e 'HLA class II' \
| less \
| datamash -s -g 1,2 unique 3,4 countunique 3,4 \
| less \
| awk -F'\t' '$NF>1' OFS='\t' \
| less \
>toexclude_HLAcount_tcellandmhcligand_joined.tsv ;
head -3 toexclude_HLAcount_tcellandmhcligand_joined.tsv ;
wc -l toexclude_HLAcount_tcellandmhcligand_joined.tsv ;
## 243 peptides found!

## let's check HLA distribution of excluded peptides:
cut -f2 toexclude_HLAcount_tcellandmhcligand_joined.tsv \
| sort \
| uniq -c ;
## keywords 'HLA class II', 'HLA-DR' and 'HLA-DR4' are the majority
### before excluding peptides from the whole process, we must check if they are found with non-conflicting individual HLA data:
less toexclude_HLAcount_tcellandmhcligand_joined.tsv \
| datamash -s -g 1 unique 2 countunique 2 \
| less \
| grep -e 'HLA class II' -e 'HLA-DR,' -e 'HLA-DR4' \
| awk -F'\t' '$NF>1' OFS='\t' \
| cat -n ;
## 2 peptides to check (the other 2 are 'HLA class II,HLA-DR')
grep -w 'DIKVQFQSGGNNSPA' tcellepitopes_classII_mhcligandandtcell_joined.tsv ;
## 'DIKVQFQSGGNNSPA' will only be excluded for 'HLA-DRB1*04:01', not from the whole process, as there are other individual HLA points
grep -w 'LRPTFDTRLMRLEDEMKEGR' tcellepitopes_classII_mhcligandandtcell_joined.tsv ;
## same for this peptide -> will only be excluded for 'HLA-DRB1*13:01', not from the whole process, as there are other individual HLA points
grep 'DR4' toexclude_HLAcount_tcellandmhcligand_joined.tsv ;
## 2 peptides to check
grep -w -e 'DRTRKPFQSVIADTGISVSE' -e 'FAGIEAAASAIQGNV' toexclude_HLAcount_tcellandmhcligand_joined.tsv ;
grep -w -e 'DRTRKPFQSVIADTGISVSE' -e 'FAGIEAAASAIQGNV' tcellepitopes_classII_mhcligandandtcell_joined.tsv ;
## the peptide 'FAGIEAAASAIQGNV' must be excluded from immunoinformatics as whole, as all data come from HLA-DR4!
## conclusion: all peptides with keywords 'HLA class II', 'HLA-DR' and 'HLA-DR4' must be excluded from the whole process, unless individual HLA data is found (as we already checked for the possibility of conflicting individual HLA data)!! Peptides with conflicting 4-digit HLA resolution will be also checked for other non-conflicting HLA data!
less toexclude_HLAcount_tcellandmhcligand_joined.tsv \
| awk -F'\t' 'NR==FNR {pepHLA[$1 FS $2] ; next} !($1 FS $2 in pepHLA)' OFS='\t' /dev/stdin <(grep '\-DR' tcellepitopes_classII_mhcligandandtcell_joined.tsv | less) \
| less \
| awk -F'\t' 'NR==FNR {peptide[$1] ; next} !($1 in peptide)' OFS='\t' /dev/stdin toexclude_HLAcount_tcellandmhcligand_joined.tsv \
| less \
>toexclude_allHLAs_tcellandmhcligand_joined.tsv ;
head -3 toexclude_allHLAs_tcellandmhcligand_joined.tsv ;
wc -l toexclude_allHLAs_tcellandmhcligand_joined.tsv ;
## 151 peptides to exclude from the whole process!
grep -w 'FAGIEAAASAIQGNV' toexclude_allHLAs_tcellandmhcligand_joined.tsv ;
## peptide found, so we can safely exclude global keywords next...
cut -f2 toexclude_allHLAs_tcellandmhcligand_joined.tsv \
| sort \
| uniq -c ;
## almost all peptides to exclude come from 'HLA class II', 'HLA-DR' and 'HLA-DR4'!
### 8 peptides to exclude come from 2 4-digit HLAs (HLA-DRB1*07:01 and HLA-DRB1*09:01)!
awk -F'\t' 'NR==FNR {peptide[$1] ; next} !($1 in peptide)' OFS='\t' toexclude_allHLAs_tcellandmhcligand_joined.tsv toexclude_HLAcount_tcellandmhcligand_joined.tsv \
| less \
| awk -F'\t' '$2!="HLA class II" && $2!="HLA-DR" && $2!="HLA-DR4"' OFS='\t' \
| less \
>toexclude_specificHLAs_tcellandmhcligand_joined.tsv ;
head -3 toexclude_specificHLAs_tcellandmhcligand_joined.tsv ;
wc -l toexclude_specificHLAs_tcellandmhcligand_joined.tsv ;
## 27 peptide/HLA pairs not to be considered after immunoinformatics predictions!!
cat toexclude_allHLAs_tcellandmhcligand_joined.tsv toexclude_specificHLAs_tcellandmhcligand_joined.tsv \
| less \
| wc -l ;
## 178 records -> not the 243 found before, so we will ignore a bit all these last steps and consider the final exclusion list as toexclude_HLAcount_tcellandmhcligand_joined.tsv in peptide/HLA pairs!!!
### the alternative would be to consider discordant peptides on general groups such as 'HLA class II' -> this approach is more conservative...


echo "Get final T cell epitope list to test" ;
#---------------------------------------------------------------------------------------  
less tcellepitopes_classII_tcellassays_joined.tsv \
| grep -e 'HLA class II' -e '-DR' \
| less \
| awk -F'\t' 'NR==FNR {pepHLA[$1 FS $2] ; next} !($1 FS $2 in pepHLA)' OFS='\t' toexclude_HLAcount_tcellandmhcligand_joined.tsv /dev/stdin \
| sed -e 's|HLA-DRA\*01:01/|HLA-|g' \
| less \
>concordantstatus_classII_tcellassays_joined.tsv ;
head -3 concordantstatus_classII_tcellassays_joined.tsv ;
wc -l concordantstatus_classII_tcellassays_joined.tsv ;
## 2199 entries!
less concordantstatus_classII_tcellassays_joined.tsv \
| datamash -s -g 1,2 unique 5 countunique 5 \
| less \
| awk -F'\t' '$NF>1' OFS='\t' ;
## no peptide/HLA pair with discordant status, confirmed!
less concordantstatus_classII_tcellassays_joined.tsv \
| datamash -s -g 1 unique 2 countunique 2 unique 5 countunique 5 \
| less \
| awk -F'\t' '$NF>1' OFS='\t' \
| cat -n ;
## 16 peptides with different status (due to different HLA restrictions...)
### some peptides might need to be excluded, due to discordant status on different (but equivalent) general group terms such as HLA class II,HLA-DR...
#### the solution is that, if a general term is present, consider only the HLAs of the least general term (e.g. HLA-DR) after immunoinformatics predictions!
grep -w 'VVAEKVRNLPAGHGL' concordantstatus_classII_tcellassays_joined.tsv ;
## this peptide might migrate to positive side (HLA-DR positive but HLA class II negative and no further HLA restriction data...)!

cut -f2 concordantstatus_classII_tcellassays_joined.tsv \
| sort \
| uniq -c ;
## 1353 'HLA class II' + 246 'HLA-DR'! So, the great majority of peptides do not have 4-digit HLA-restriction...
less concordantstatus_classII_tcellassays_joined.tsv \
| awk -F'\t' '$2!="HLA-DRB1"' OFS='\t' \
| grep 'DRB[1-5]' \
| less \
| awk -F'\t' '{print $NF}' \
| sort \
| uniq -c ;
## 419 entries -> 380 positive + 39 negative
### number of negative peptides is not suficient for a robust ROC with 4-digit HLAs...
awk -F'\t' 'length($1)<9 {print $1}' concordantstatus_classII_tcellassays_joined.tsv \
| sort -u \
| wc -l ;
## only 1 peptide with length < 9 aa!!
grep 'positive' concordantstatus_classII_tcellassays_joined.tsv \
| awk -F'\t' 'length($1)>8 {print $1}' \
| sort -u \
| wc -l ;
## 1037 unique positive peptides!
grep 'negative' concordantstatus_classII_tcellassays_joined.tsv \
| awk -F'\t' 'length($1)>8 {print $1}' \
| sort -u \
| wc -l ;
## 219 unique negative peptides! (it is not balanced but at least it might be sufficient for a ROC or to get tendencies with nonparametric statistics...)
awk -F'\t' 'length($1)>8 {print $1}' concordantstatus_classII_tcellassays_joined.tsv \
| sort -u \
| less \
>peps_concordantstatus_classII_tcellassays_joined.mhc2 ;
head -3 peps_concordantstatus_classII_tcellassays_joined.mhc2 ;
wc -l peps_concordantstatus_classII_tcellassays_joined.mhc2 ;
## 1240 peptides to test in immunoinformatics!!
### value is lower than 1063+283=1346 because the same peptide can be simultaneously positive for some HLAs and negative for others...


echo "Compress and remove large files" ;
#---------------------------------------------------------------------------------------
du -shc *.* | sort -rh ;
## ≃12MB total files!
ls tcellepitopes_classII_*tive_*.csv* \
| less \
>compressedandremovedfiles_mhc2iedbepitopes.txt ;
less compressedandremovedfiles_mhc2iedbepitopes.txt \
| while read file ;
  do echo filetocompress="$file" ;
    tar -czvf "$file".tar.gz "$file" ;
    rm "$file" ;
done ;
du -shc *.* | sort -rh ;
## 1.6MB total size after compression of 4 files!
